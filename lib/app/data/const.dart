import 'package:flutter/material.dart';

const mainColor = Color.fromARGB(255, 125, 214, 255);
const mainColor2 = Color.fromARGB(255, 91, 196, 245);
const mainColor3 = Color.fromARGB(255, 24, 154, 214);
const mainColorB1 = Color.fromARGB(255, 75, 79, 82);
const mainColorB2 = Color.fromARGB(255, 61, 63, 65);
const mainColorB3 = Color.fromARGB(255, 46, 47, 48);
const mainColorW1 = Color.fromARGB(255, 205, 216, 228);
const mainColorW2 = Color.fromARGB(255, 199, 205, 211);
const mainColorW3 = Color.fromARGB(255, 172, 174, 175);
const mainColorW4 = Color.fromARGB(255, 175, 175, 175);
