import 'package:flutter/material.dart';
import 'package:get/get.dart';

showInSnackBar(String title, String content, bool isError) {
  Get.snackbar(title, content,
      backgroundColor: Colors.white70,
      titleText: Text(
        title,
        style: TextStyle(color: isError ? Colors.red : Colors.black87),
      ));
}
