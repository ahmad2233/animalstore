import 'package:flutter/material.dart';

inputDecoration(String? hintText) {
  return InputDecoration(
      fillColor: Colors.transparent,
      filled: true,
      hintText: hintText ?? '',
      hintStyle: TextStyle(
        color: Colors.grey[400],
      ),
      contentPadding: const EdgeInsets.symmetric(horizontal: 20.0),
      border: border(),
      enabledBorder: border(),
      focusedBorder: focusBorder(),
      errorStyle: const TextStyle(height: 0.0, fontSize: 0.0));
}

border() {
  return const OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(30.0),
    ),
    borderSide: BorderSide(
      color: Colors.grey,
      width: 0.0,
    ),
  );
}

focusBorder() {
  return OutlineInputBorder(
    borderRadius: const BorderRadius.all(
      Radius.circular(30.0),
    ),
    borderSide: BorderSide(
      color: Colors.black.withOpacity(0.5),
      width: 1.0,
    ),
  );
}
