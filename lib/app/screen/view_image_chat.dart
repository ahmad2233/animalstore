import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';

class ViewImageChat extends StatelessWidget {
  final String? imageUrl;

  const ViewImageChat({Key? key, this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.passthrough,
        children: [
          SizedBox.expand(
            child: ClipRect(
              child: ImageFiltered(
                imageFilter: ImageFilter.blur(
                  sigmaX: 10.0,
                  sigmaY: 10.0,
                ),
                child: CachedNetworkImage(
                  imageUrl: imageUrl!,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
          CachedNetworkImage(
            imageUrl: imageUrl!,
            fit: BoxFit.contain,
          )
        ],
      ),
    );
  }
}
