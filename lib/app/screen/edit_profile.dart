import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/services/validation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../tools/text_field_decoration.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({Key? key}) : super(key: key);

  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: inputDecoration(
                  'Full Name',
                ),
                validator: Validations.validateUserName,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: inputDecoration(
                  'Location',
                ),
                validator: Validations.validateCountryName,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: inputDecoration(
                  'Mobile Number',
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ], // Only numbers can be entered
                validator: Validations.validateMobile,
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: mainColor,
                  padding:
                      const EdgeInsets.symmetric(vertical: 12, horizontal: 40),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  textStyle: const TextStyle(fontWeight: FontWeight.bold)),
              onPressed: () async {},
              child: const Text(
                "Submit",
              ),
            )
          ],
        ),
      ),
    );
  }
}
