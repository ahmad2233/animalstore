import 'package:get/get.dart';

import '../modules/account/account_binding.dart';
import '../modules/account/account_view.dart';
import '../modules/boNavBar/bo_nav_bar_binding.dart';
import '../modules/boNavBar/bo_nav_bar_view.dart';
import '../modules/chat/chat_binding.dart';
import '../modules/chat/chat_view.dart';
import '../modules/choise/choise_binding.dart';
import '../modules/choise/choise_view.dart';
import '../modules/conversation/conversation_binding.dart';
import '../modules/conversation/conversation_view.dart';
import '../modules/create_post/create_post_binding.dart';
import '../modules/create_post/create_post_view.dart';
import '../modules/home//home_view.dart';
import '../modules/home/home_binding.dart';
import '../modules/login/login_binding.dart';
import '../modules/login/login_view.dart';
import '../modules/mypets/mypets_binding.dart';
import '../modules/mypets/mypets_view.dart';
import '../modules/signup/signup_binding.dart';
import '../modules/signup/signup_view.dart';
import '../modules/splash/splash_binding.dart';
import '../modules/splash/splash_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.BO_NAV_BAR,
      page: () => BoNavBarView(),
      binding: BoNavBarBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => SignupView(),
      binding: SignupBinding(),
    ),
    GetPage(
      name: _Paths.CREATE_POST,
      page: () => CreatePostView(),
      binding: CreatePostBinding(),
    ),
    GetPage(
      name: _Paths.ACCOUNT,
      page: () => AccountView(),
      binding: AccountBinding(),
    ),
    GetPage(
      name: _Paths.CHOISE,
      page: () => ChoiseView(),
      binding: ChoiseBinding(),
    ),
    GetPage(
      name: _Paths.MYPETS,
      page: () => MypetsView(),
      binding: MypetsBinding(),
    ),
    GetPage(
      name: _Paths.CHAT,
      page: () => ChatView(),
      binding: ChatBinding(),
    ),
    GetPage(
      name: _Paths.CONVERSATION,
      page: () => ConversationView(),
      binding: ConversationBinding(),
    ),
  ];
}
