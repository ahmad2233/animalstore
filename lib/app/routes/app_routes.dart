// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH = _Paths.SPLASH;
  static const BO_NAV_BAR = _Paths.BO_NAV_BAR;
  static const LOGIN = _Paths.LOGIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const CREATE_POST = _Paths.CREATE_POST;
  static const ACCOUNT = _Paths.ACCOUNT;
  static const CHOISE = _Paths.CHOISE;
  static const MYPETS = _Paths.MYPETS;
  static const CHAT = _Paths.CHAT;
  static const CONVERSATION = _Paths.CONVERSATION;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SPLASH = '/splash';
  static const BO_NAV_BAR = '/bonavbar';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const CREATE_POST = '/create-post';
  static const ACCOUNT = '/account';
  static const CHOISE = '/choise';
  static const MYPETS = '/mypets';
  static const CHAT = '/chat';
  static const CONVERSATION = '/conversation';
}
