import 'package:animal_store/app/data/const.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:timeago/timeago.dart' as timeago;

import '../screen/view_image_chat.dart';
import '../tools/message_type.dart';

class ChatBubble extends StatefulWidget {
  final String? message;
  final MessageType? type;
  final Timestamp? time;
  final bool? isMe;

  const ChatBubble({
    this.message,
    this.time,
    this.isMe,
    this.type,
  });

  @override
  _ChatBubbleState createState() => _ChatBubbleState();
}

class _ChatBubbleState extends State<ChatBubble> {
  Color chatBubbleColor() {
    if (widget.isMe!) {
      return mainColor;
    } else {
      if (Theme.of(context).brightness == Brightness.dark) {
        return mainColor3;
      } else {
        return mainColor3;
      }
    }
  }

  Color chatBubbleReplyColor() {
    if (Theme.of(context).brightness == Brightness.dark) {
      return mainColor;
    } else {
      return mainColor3;
    }
  }

  @override
  Widget build(BuildContext context) {
    final align =
        widget.isMe! ? CrossAxisAlignment.end : CrossAxisAlignment.start;
    final radius = widget.isMe!
        ? const BorderRadius.only(
            topLeft: Radius.circular(5.0),
            bottomLeft: Radius.circular(5.0),
            bottomRight: Radius.circular(10.0),
          )
        : const BorderRadius.only(
            topRight: Radius.circular(5.0),
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(5.0),
          );
    return Column(
      crossAxisAlignment: align,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(3.0),
          padding: const EdgeInsets.all(5.0),
          decoration: BoxDecoration(
            color: chatBubbleColor(),
            borderRadius: radius,
          ),
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width / 1.3,
            minWidth: 20.0,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: widget.type == MessageType.TEXT ? 11 : 0),
                child: widget.type == MessageType.TEXT
                    ? Text(
                        widget.message!,
                        textAlign: _checkForArabicLetter(
                          widget.message!,
                        )
                            ? TextAlign.left
                            : TextAlign.right,
                        textDirection: _checkForArabicLetter(
                          widget.message!,
                        )
                            ? TextDirection.rtl
                            : TextDirection.ltr,
                        style: TextStyle(
                          color: widget.isMe!
                              ? Colors.grey[850]
                              : Colors.grey[850],
                        ),
                      )
                    : GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            CupertinoPageRoute(
                              builder: (_) => ViewImageChat(
                                imageUrl: widget.message,
                              ),
                            ),
                          );
                        },
                        child: CachedNetworkImage(
                          imageUrl: "${widget.message}",
                          height: 200,
                          width: MediaQuery.of(context).size.width / 1.3,
                          fit: BoxFit.cover,
                        ),
                      ),
              ),
            ],
          ),
        ),
        Padding(
          padding: widget.isMe!
              ? const EdgeInsets.only(
                  right: 10.0,
                  bottom: 10.0,
                )
              : const EdgeInsets.only(
                  left: 10.0,
                  bottom: 10.0,
                ),
          child: Text(
            timeago.format(widget.time!.toDate()),
            style: const TextStyle(
              color: mainColorB2,
              fontSize: 10.0,
            ),
          ),
        ),
      ],
    );
  }

  bool _checkForArabicLetter(String text) {
    final arabicRegex = RegExp(r'[ء-ي-_ \.]*$');
    final englishRegex = RegExp(r'[a-zA-Z ]');
    return text.contains(arabicRegex) && !text.startsWith(englishRegex);
  }
}
