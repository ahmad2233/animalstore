import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/modules/create_post/post_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../routes/app_pages.dart';
import '../services/firebase_auth.dart';
import '../services/firestore.dart';
import '../tools/indector.dart';

class PetsDetalis extends StatefulWidget {
  final PostModel? postModel;

  const PetsDetalis({Key? key, this.postModel}) : super(key: key);

  @override
  State<PetsDetalis> createState() => _PetsDetalisState();
}

class _PetsDetalisState extends State<PetsDetalis> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   leading: IconButton(
      //       onPressed: () {
      //         Get.back();
      //       },
      //       icon: const Icon(
      //         Icons.arrow_back_outlined,
      //         color: Colors.transparent,
      //       )),
      //   backgroundColor: Colors.white,
      //   elevation: 0,
      // ),
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                Stack(
                  children: [
                    SizedBox(
                      height: Get.height / 3,
                      width: Get.width,
                      child: CachedNetworkImage(
                        imageUrl: widget.postModel!.photoUrl!,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(45),
                              // topLeft: Radius.circular(20),
                              // topRight: Radius.circular(20),
                            ),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        fit: BoxFit.fitWidth,
                        placeholder: (context, url) =>
                            circularProgress(context),
                        errorWidget: (context, url, error) => const Center(
                          child: Text(
                            'Unable to load Image',
                            style: TextStyle(fontSize: 10.0),
                          ),
                        ),
                      ),
                      // child: Image.network(
                      //   widget.postModel!.photoUrl!,
                      //   fit: BoxFit.fill,
                      // ),
                    ),
                    Visibility(
                      // visible: widget.postModel.,
                      child: Align(
                        child: Text('Price ${widget.postModel!.price} \$'),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Text(
                            widget.postModel!.name!,
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 35,
                                fontWeight: FontWeight.w900),
                          ),
                          const Spacer(),
                          Text(widget.postModel!.location!)
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Text(widget.postModel!.type ?? ''),
                          const SizedBox(
                            width: 5,
                          ),
                          widget.postModel!.gendre!
                              ? const Icon(Icons.male)
                              : const Icon(Icons.male)
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          buildContainer('age', widget.postModel!.age!),
                          buildContainer('hight', widget.postModel!.length!),
                          buildContainer('wight', widget.postModel!.wight!),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      const Text(
                        'About',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 35,
                            fontWeight: FontWeight.w900),
                      ),
                      const SizedBox(
                        height: 14,
                      ),
                      Text(
                        widget.postModel!.description!,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.w900),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Visibility(
              visible: currentUserId() != widget.postModel!.owner!,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: Get.height / 6,
                  width: Get.width,
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          launchCaller(widget.postModel!.phone ?? '');
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: mainColorW3.withOpacity(0.5),
                            borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(20),
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          width: Get.width / 3,
                          height: 60,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text('Call'),
                              SizedBox(
                                width: 5,
                              ),
                              Icon(
                                Icons.call_rounded,
                              )
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          startChat(widget.postModel!.owner!);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            color: mainColor2,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20),
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          width: Get.width / 3,
                          height: 60,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                'Chat',
                                style: TextStyle(color: Colors.white),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Icon(Icons.chat_bubble_outline_outlined,
                                  color: Colors.white)
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  launchCaller(String phoneNumber) async {
    String url = "tel:$phoneNumber";
    // url = "tel:$phoneNumber";
    if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  Container buildContainer(String text, String text2) {
    return Container(
      height: 66,
      width: 66,
      decoration: BoxDecoration(
        color: mainColorW3.withOpacity(0.5),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(text),
          const SizedBox(
            height: 10,
          ),
          Text(text2)
        ],
      ),
    );
  }

  startChat(String resciver) async {
    String sender = currentUserId()!;
    print(sender);
    print(resciver);
    QuerySnapshot snapshot = await chatRef
        .where('re', isEqualTo: sender)
        .where('st', isEqualTo: resciver)
        .get();
    if (snapshot.docs.isNotEmpty) {
      Get.toNamed(Routes.CONVERSATION, arguments: [resciver, '', '0']);
    } else {
      QuerySnapshot snapshot = await chatRef
          .where('st', isEqualTo: sender)
          .where('re', isEqualTo: resciver)
          .get();
      if (snapshot.docs.isEmpty) {
        Get.toNamed(Routes.CONVERSATION, arguments: [resciver, '', '1']);
      } else {
        Get.toNamed(Routes.CONVERSATION,
            arguments: [resciver, snapshot.docs[0].id, '0']);
      }
    }
  }

  currentUserId() {
    return firebaseAuth.currentUser?.uid;
  }
}
