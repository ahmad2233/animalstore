import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/routes/app_pages.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:timeago/timeago.dart' as timeago;
import '../modules/login/user_model.dart';
import '../services/firestore.dart';
import '../tools/message_type.dart';

class ChatItem extends StatelessWidget {
  final String? userId;
  final Timestamp? time;
  final String? msg;
  final int? messageCount;
  final String? chatId;
  final MessageType? type;
  final String? currentUserId;

  const ChatItem(
      {Key? key,
      this.userId,
      this.time,
      this.msg,
      this.messageCount,
      this.chatId,
      this.type,
      this.currentUserId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: usersRef.doc('$userId').snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          DocumentSnapshot? documentSnapshot =
              snapshot.data as DocumentSnapshot;
          UsersModel user = UsersModel.fromJson(
              documentSnapshot.data() as Map<String, dynamic>);

          return Stack(
            children: [
              ListTile(
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                leading: Stack(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: CachedNetworkImageProvider(
                        '${user.photo}',
                      ),
                      backgroundColor: mainColor,
                      radius: 25.0,
                    ),
                    Positioned(
                      bottom: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(6.0),
                        ),
                        height: 15,
                        width: 15,
                        child: Center(
                          child: Container(
                            decoration: BoxDecoration(
                              color: user.isOnline ?? false
                                  ? const Color(0xff00d72f)
                                  : Colors.grey,
                              borderRadius: BorderRadius.circular(6),
                            ),
                            height: 11,
                            width: 11,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                title: Text(
                  '${user.username}',
                  maxLines: 1,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(
                  type == MessageType.IMAGE ? "IMAGE" : "$msg",
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                trailing: Text(
                  timeago.format(time!.toDate()),
                  style: const TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 11,
                  ),
                ),
                onTap: () async {
                  // await Navigator.of(context, rootNavigator: true).push(
                  //   CupertinoPageRoute(
                  //     builder: (BuildContext context) {
                  //       return ConversationView(
                  //           // userId: userId,
                  //           // docChatId: chatId,
                  //           // chatId: 'oldchat',
                  //           );
                  //     },
                  //   ),
                  // );
                  Get.toNamed(Routes.CONVERSATION, arguments: [
                    userId,
                    chatId,
                    'oldchat',
                  ]);
                },
              ),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  height: 25,
                  width: 25,
                  child: Center(
                    child: buildCounter(context),
                  ),
                ),
              )
            ],
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  buildCounter(BuildContext context) {
    return StreamBuilder(
      stream: messageBodyStream(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          // DocumentSnapshot snap = snapshot;
          // print(snapshot.data.docs);
          // Map usersReads = snap.get('reads') ?? {};
          // int readCount = usersReads[currentUserId] ?? 0;
          int counter = snapshot.data.docs.length;
          if (counter == 0) {
            return Container(
              color: Colors.transparent,
            );
          } else {
            return Container(
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.secondary,
                  shape: BoxShape.circle),
              child: Text(
                "$counter",
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                ),
                textAlign: TextAlign.center,
              ),
            );
          }
        } else {
          return const SizedBox();
        }
      },
    );
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> messageBodyStream() {
    return chatRef
        .doc(chatId)
        .collection('messages')
        .where('senderUid', isEqualTo: userId)
        .where('isread', isEqualTo: false)
        .snapshots();
  }
}
