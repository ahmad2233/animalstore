import 'package:animal_store/app/modules/create_post/post_model.dart';
import 'package:animal_store/app/tools/indector.dart';
import 'package:animal_store/app/widget.dart/pet_detalis.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:timeago/timeago.dart' as timeago;

class PetsWidget extends StatelessWidget {
  final PostModel? postModel;

  const PetsWidget({Key? key, this.postModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PetsDetalis(
                      postModel: postModel,
                    )));
      },
      child: Container(
        height: Get.height / 4,
        width: Get.width,
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  // image: DecorationImage(
                  //     image: NetworkImage(postModel!.photoUrl!),
                  //     fit: BoxFit.fitHeight)
                ),
                child: CachedNetworkImage(
                  imageUrl: postModel!.photoUrl!,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      // shape: BoxShape.e,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  fit: BoxFit.fitWidth,
                  placeholder: (context, url) => circularProgress(context),
                  errorWidget: (context, url, error) => const Center(
                    child: Text(
                      'Unable to load Image',
                      style: TextStyle(fontSize: 10.0),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                height: Get.height / 6,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20))),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: const <Widget>[
                            // but it or all widget with it in Expanded .....,
                            Spacer(),
                            Icon(
                              Icons.male,
                              size: 20,
                            )
                          ],
                        ),
                        ExpandableText(
                          postModel!.name!,
                          expandText: 'show more',
                          collapseText: 'show less',
                          maxLines: 2,
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        ExpandableText(
                          postModel!.type ?? '',
                          expandText: 'more',
                          collapseText: 'less',
                          maxLines: 2,
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        Text(
                          timeago.format(postModel!.creatAt!.toDate()),
                          style:
                              const TextStyle(fontSize: 16, color: Colors.grey),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        Row(
                          children: <Widget>[
                            const Icon(Icons.location_on),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(postModel!.location!)
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
