import 'package:animal_store/app/modules/create_post/post_model.dart';
import 'package:animal_store/app/tools/indector.dart';
import 'package:animal_store/app/widget.dart/pet_detalis.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../services/firestore.dart';

class MypetsWidget extends StatefulWidget {
  final PostModel? postModel;
  final String? id;

  const MypetsWidget({Key? key, this.postModel, this.id}) : super(key: key);

  @override
  State<MypetsWidget> createState() => _MypetsWidgetState();
}

class _MypetsWidgetState extends State<MypetsWidget> {
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PetsDetalis(
                      postModel: widget.postModel,
                    )));
      },
      child: ModalProgressHUD(
        inAsyncCall: isLoading,
        progressIndicator: circularProgress(context),
        child: Stack(
          children: [
            Container(
              height: Get.height / 4,
              width: Get.width,
              margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        // image: DecorationImage(
                        //     image: NetworkImage(postModel!.photoUrl!),
                        //     fit: BoxFit.fitHeight)
                      ),
                      child: CachedNetworkImage(
                        imageUrl: widget.postModel!.photoUrl!,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            // shape: BoxShape.e,
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        fit: BoxFit.fitWidth,
                        placeholder: (context, url) =>
                            circularProgress(context),
                        errorWidget: (context, url, error) => const Center(
                          child: Text(
                            'Unable to load Image',
                            style: TextStyle(fontSize: 10.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: Get.height / 6,
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: const <Widget>[
                                  // but it or all widget with it in Expanded .....,
                                  Spacer(),
                                  Icon(
                                    Icons.male,
                                    size: 20,
                                  )
                                ],
                              ),
                              ExpandableText(
                                widget.postModel!.name!,
                                expandText: 'show more',
                                collapseText: 'show less',
                                maxLines: 2,
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              ExpandableText(
                                widget.postModel!.type ?? '',
                                expandText: 'more',
                                collapseText: 'less',
                                maxLines: 2,
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Text(
                                timeago.format(
                                    widget.postModel!.creatAt!.toDate()),
                                style: const TextStyle(
                                    fontSize: 16, color: Colors.grey),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Row(
                                children: <Widget>[
                                  const Icon(Icons.location_on),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text(widget.postModel!.location!)
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            IconButton(
                onPressed: () {
                  showDeleteDialog(widget.id!);
                },
                icon: const Icon(
                  Icons.delete,
                  size: 45,
                  color: Colors.red,
                ))
          ],
        ),
      ),
    );
  }

  showDeleteDialog(String docID) {
    Get.dialog(
      Center(
        child: Container(
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(25)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  height: 100,
                  width: 100,
                  padding: const EdgeInsets.all(25),
                  decoration: const BoxDecoration(
                    color: Colors.red,
                    shape: BoxShape.circle,
                  ),
                  child: const Icon(
                    Icons.info,
                    color: Colors.white,
                    size: 35,
                  )),
              const SizedBox(
                height: 15,
              ),
              const Text(
                'Are you sure',
                style: TextStyle(fontSize: 18),
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.red,
                          padding: const EdgeInsets.symmetric(
                              vertical: 12, horizontal: 40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          textStyle:
                              const TextStyle(fontWeight: FontWeight.bold)),
                      onPressed: () async {
                        // Navigator.pop(context);
                        await deletePost(docID);
                        Get.back();
                      },
                      child: const Text(
                        'OK',
                        style: TextStyle(color: Colors.white),
                      )),
                  const SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          padding: const EdgeInsets.symmetric(
                              vertical: 12, horizontal: 40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          textStyle:
                              const TextStyle(fontWeight: FontWeight.bold)),
                      onPressed: () {
                        Get.back();
                      },
                      child: const Text(
                        'Cancle',
                        style: TextStyle(color: Colors.red),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  deletePost(String docID) async {
    setState(() {
      isLoading = true;
    });
    await postsRef.doc(docID).delete();
    await FirebaseStorage.instance
        .refFromURL(widget.postModel!.photoUrl!)
        .delete();
    // Navigator.pop(context);
  }
}
