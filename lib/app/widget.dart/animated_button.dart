import 'package:flutter/material.dart';

import '../data/const.dart';

class AnimeatedButtons extends StatefulWidget {
  final VoidCallback? ontap;
  final String? text;

  const AnimeatedButtons({Key? key, this.ontap, this.text}) : super(key: key);
  @override
  _AnimeatedButtonsState createState() => _AnimeatedButtonsState();
}

class _AnimeatedButtonsState extends State<AnimeatedButtons>
    with TickerProviderStateMixin {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: mainColor, borderRadius: BorderRadius.circular(24)),
      child: Center(
        child: InkWell(
          // highlightColor: Colors.transparent,
          // splashColor: Colors.transparent,
          onHighlightChanged: (value) {
            setState(() {
              isTapped = value;
            });
          },
          onTap: widget.ontap,
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 500),
            curve: Curves.fastLinearToSlowEaseIn,
            height: isTapped ? 50 : 44,
            child: Center(
              child: Text(
                widget.text!,
                style: TextStyle(
                  color: Colors.white.withOpacity(0.7),
                  fontWeight: FontWeight.w500,
                  fontSize: 19,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
