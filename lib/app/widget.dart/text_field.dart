import 'package:flutter/material.dart';

class TextFieldWidget extends StatefulWidget {
  final Icon? icon;
  final String? hintText;
  final String? initialValue;
  final bool? isPassword;
  final bool? isEmail;
  final bool isText;
  final bool? enabled;
  final TextEditingController? controller;
  final bool? readOnly;
  final FocusNode? focusNode, nextFocusNode;
  final VoidCallback? submitAction;
  final FormFieldValidator<String>? validateFunction;
  final void Function(String)? onSaved, onChange;
  final VoidCallback? ontap;
  final TextInputAction? textInputAction;
  final double? raduis;

  const TextFieldWidget(
      {Key? key,
      this.icon,
      this.hintText,
      this.isPassword = false,
      this.isEmail = false,
      this.isText = false,
      this.enabled = true,
      this.controller,
      this.readOnly = false,
      this.focusNode,
      this.nextFocusNode,
      this.submitAction,
      this.validateFunction,
      this.onSaved,
      this.onChange,
      this.ontap,
      this.initialValue,
      this.textInputAction,
      this.raduis = 30.0})
      : super(key: key);

  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  String? error;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            child: TextFormField(
              readOnly: widget.readOnly!,
              onTap: widget.ontap,
              textCapitalization: TextCapitalization.sentences,
              initialValue: widget.initialValue,
              enabled: widget.enabled,
              onChanged: (val) {
                error = widget.validateFunction!(val);
                setState(() {});
                widget.onSaved!(val);
              },
              style: const TextStyle(
                fontSize: 15.0,
              ),
              key: widget.key,
              controller: widget.controller,
              obscureText: widget.isPassword!,
              textInputAction: widget.textInputAction,
              keyboardType: widget.isText
                  ? TextInputType.text
                  : widget.isEmail!
                      ? TextInputType.emailAddress
                      : TextInputType.visiblePassword,
              validator: widget.validateFunction,
              onSaved: (val) {
                error = widget.validateFunction!(val);
                widget.onSaved!(val!);
              },
              // textInputAction:
              focusNode: widget.focusNode,
              onFieldSubmitted: (String term) {
                if (widget.nextFocusNode != null) {
                  widget.focusNode!.unfocus();
                  FocusScope.of(context).requestFocus(widget.nextFocusNode);
                } else {
                  widget.submitAction!();
                }
              },
              decoration: InputDecoration(
                  prefixIcon: widget.icon,
                  // suffixIcon: Icon(
                  //   suffix,
                  //   size: 15.0,
                  // ),
                  fillColor: Colors.transparent,
                  filled: true,
                  hintText: widget.hintText,
                  hintStyle: TextStyle(
                    color: Colors.grey[400],
                  ),
                  contentPadding: const EdgeInsets.symmetric(horizontal: 20.0),
                  border: border(context, widget.raduis),
                  enabledBorder: border(context, widget.raduis),
                  focusedBorder: focusBorder(context, widget.raduis),
                  errorStyle: const TextStyle(height: 0.0, fontSize: 0.0)),
            ),
          ),
          const SizedBox(height: 5.0),
          Visibility(
            visible: error != null,
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Text(
                '$error',
                style: TextStyle(
                  color: Colors.red[700],
                  fontSize: 12.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  border(BuildContext context, double? radius) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(radius!),
      ),
      borderSide: const BorderSide(
        color: Colors.grey,
        width: 0.0,
      ),
    );
  }

  focusBorder(BuildContext context, double? radius) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(radius!),
      ),
      borderSide: BorderSide(
        color: Colors.black.withOpacity(0.5),
        width: 1.0,
      ),
    );
  }
}
