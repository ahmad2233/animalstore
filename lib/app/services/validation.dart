import 'package:get/get.dart';

class Validations {
  static String? validateEmail(
    String? value,
  ) {
    if (value!.isEmpty) return 'Email is required.';
    final RegExp nameExp = RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    if (!nameExp.hasMatch(value)) return 'Invalid email address';
    return null;
  }

  static String? validateUserName1(String? val) {
    if (val!.isEmpty) return 'FirstName is Required.';
    if (val.length < 3 || val.length > 10) {
      return 'Please enter a valid FirstName';
    }
    GetUtils.isUsername(val) ? null : "Please enter a valid FirstName";
    return null;
  }

  static String? validateUserName2(String? val) {
    if (val!.isEmpty) return 'UserName is Required.';
    if (val.length < 4 || val.length > 30) {
      return 'Please enter a valid UserName';
    }
    GetUtils.isUsername(val) ? null : "Please enter a valid LastName";
    return null;
  }

  static String? validateUserName(String? val) {
    if (val!.isEmpty) return 'UserName is Required.';
    if (val.length < 4 || val.length > 25) {
      return 'Please enter a valid UserName';
    }
    GetUtils.isUsername(val) ? null : "Please enter a valid LastName";
    return null;
  }

  static String? validatePassword(String? value) {
    if (value!.isEmpty || value.length < 6) {
      return 'Please enter a valid password.';
    }
    if (value.length > 32) {
      return 'too Long Password';
    }
    return null;
  }

  static String? validateCountryName(String? value) {
    if (value!.isEmpty) return 'Country is Required.';
    final RegExp nameExp = RegExp(r'^[A-za-zğüşöçİĞÜŞÖÇ ]+$');
    if (!nameExp.hasMatch(value)) {
      return 'Please enter only alphabetical characters.';
    }
    return null;
  }

  static String? validateMobile(String? value) {
    if (value!.isEmpty) return 'Phone is required.';
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = RegExp(pattern);
    if (value.isEmpty) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value) ||
        value.length > 15 ||
        value.length < 9) {
      return 'Please enter valid mobile number';
    }
    return null;
  }

  static String? valideNothing(String? value) {
    return null;
  }
}
