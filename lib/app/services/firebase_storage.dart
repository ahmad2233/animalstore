// Storage refs
import 'package:animal_store/app/services/firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

Reference userProfile = storage.ref().child('Userprofile');
Reference marketprofile = storage.ref().child('Marketprofile');
Reference posts = storage.ref().child('posts');
Reference randposts = storage.ref().child('randposts');
