import 'package:animal_store/app/services/firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'firebase_auth.dart';

class UserService {
  String currentUid() {
    return firebaseAuth.currentUser!.uid;
  }

  setUserStatus(bool isOnline) {
    var user = firebaseAuth.currentUser;
    if (user != null) {
      usersRef
          .doc(user.uid)
          .update({'isOnline': isOnline, 'lastSeen': Timestamp.now()});
    }
  }
}
