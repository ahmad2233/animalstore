import 'package:get/get.dart';

import './choise_controller.dart';

class ChoiseBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChoiseController>(
      () => ChoiseController(),
    );
  }
}
