import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';

import './choise_controller.dart';

class ChoiseView extends GetView<ChoiseController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'PET SHELTER',
          style: TextStyle(color: mainColor),
        ),
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: const Icon(
              Icons.arrow_back_outlined,
              color: mainColor,
            )),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(50.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.CREATE_POST);
                },
                child: Container(
                  height: Get.height / 4,
                  width: Get.width / 2,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 2, vertical: 10),
                  decoration: BoxDecoration(
                    color: mainColor3,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: SvgPicture.asset(
                          'assets/svg/sale.svg',
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Expanded(
                        flex: 1,
                        child: Text(
                          'Add Pet for sale',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Get.toNamed(Routes.MYPETS);
                },
                child: Container(
                  height: Get.height / 4,
                  width: Get.width / 2,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 2, vertical: 10),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: mainColor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: SvgPicture.asset(
                          'assets/svg/dog2.svg',
                          color: Colors.white,
                        ),
                      ),
                      // const SizedBox(
                      //   height: 4,
                      // ),
                      const Expanded(
                        flex: 1,
                        child: Text(
                          'My Pets',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
