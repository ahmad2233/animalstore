import 'package:animal_store/app/tools/snackbar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../services/firestore.dart';

class SignupController extends GetxController {
  RxString? email = ''.obs;
  RxString? pass = ''.obs;
  RxString? repass = ''.obs;
  RxString? userName = ''.obs;
  RxString? location = ''.obs;
  RxString? phone = ''.obs;
  Rx<TextEditingController> userNameController = TextEditingController().obs;
  Rx<TextEditingController> emailController = TextEditingController().obs;
  Rx<TextEditingController>? passController = TextEditingController().obs;
  Rx<TextEditingController>? repassController = TextEditingController().obs;
  Rx<TextEditingController>? phoneController = TextEditingController().obs;
  Rx<TextEditingController>? locationController = TextEditingController().obs;
  FocusNode userNameFN = FocusNode();
  FocusNode emailFN = FocusNode();
  FocusNode passFN = FocusNode();
  FocusNode repassFN = FocusNode();
  FocusNode locationFN = FocusNode();
  FocusNode phoneFN = FocusNode();
  RxBool isLoading = false.obs;
  RxBool isValid = false.obs;
  Rx<GlobalKey<FormState>> signupFormKey = GlobalKey<FormState>().obs;
  Rx<GlobalKey<ScaffoldState>> signupcaffoldKey =
      GlobalKey<ScaffoldState>().obs;

  final box = GetStorage();

  setEmail(val) {
    email?.value = val;
  }

  setLocation(val) {
    location?.value = val;
  }

  setPass(val) {
    pass?.value = val;
  }

  setPhone(val) {
    phone?.value = val;
  }

  setRePass(val) {
    repass?.value = val;
  }

  setUserName(val) {
    userName?.value = val;
    print(val);
  }

  String? validteRePass(String? val) {
    if (passController!.value.text != val) return 'Password not match';
    return null;
  }

  signup() async {
    isLoading.value = true;
    FormState? form = signupFormKey.value.currentState;
    form!.save();
    if (!form.validate()) {
      showInSnackBar(
          'Error', 'Please fix the errors in red before submitting', true);
    } else {
      try {
        final credential =
            await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email!.value,
          password: pass!.value,
        );
        await saveUserToFireSore(credential.user!);
        saveLocal();
        Get.offAllNamed('bonavbar');
      } on FirebaseAuthException catch (e) {
        if (e.code == 'weak-password') {
          showInSnackBar('Error', 'The password provided is too weak.', true);
        } else if (e.code == 'email-already-in-use') {
          showInSnackBar(
              'Error', 'The account already exists for that email.', true);
        }
      } catch (e) {
        showInSnackBar('Error', e.toString(), true);
      }
    }
    isLoading.value = false;
  }

  saveUserToFireSore(User user) async {
    await usersRef.doc(user.uid).set({
      'username': userName!.value,
      'phone': phone!.value,
      'location': location!.value,
      'time': Timestamp.now(),
      'email': email!.value,
      'photo': '',
      'isOnline': true,
      'lastSeen': Timestamp.now(),
    }).catchError((e) => print(e));
  }

  saveLocal() {
    box.write('username', userName!.value);
    box.write('phone', phone!.value);
    box.write('location', location!.value);
    box.write('email', email!.value);
  }

  @override
  void onClose() {
    userNameController.value.dispose();
    emailController.value.dispose();
    passController!.value.dispose();
    repassController!.value.dispose();
    phoneController!.value.dispose();
    locationController!.value.dispose();
    super.onClose();
  }
}
