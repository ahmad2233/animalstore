import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/tools/indector.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import '../../services/validation.dart';
import '../../widget.dart/animated_button.dart';
import '../../widget.dart/text_field.dart';
import './signup_controller.dart';

class SignupView extends GetView<SignupController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return ModalProgressHUD(
        inAsyncCall: controller.isLoading.value,
        progressIndicator: circularProgress(context),
        child: Scaffold(
          key: controller.signupcaffoldKey.value,
          body: Stack(
            children: [
              ScrollConfiguration(
                behavior: MyBehavior(),
                child: SingleChildScrollView(
                  child: Container(
                    height: Get.height,
                    width: Get.width,
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            colors: [mainColor3, Colors.white],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight)),
                    child: Center(
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 9),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 6, vertical: 9),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15)),
                        child: Obx(() {
                          return Form(
                            key: controller.signupFormKey.value,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                TextFieldWidget(
                                  controller:
                                      controller.userNameController.value,
                                  enabled: true,
                                  focusNode: controller.userNameFN,
                                  hintText: 'UserName',
                                  icon: const Icon(
                                    Icons.person_outline,
                                    color: Colors.black54,
                                  ),
                                  isText: true,
                                  textInputAction: TextInputAction.next,
                                  nextFocusNode: controller.phoneFN,
                                  validateFunction:
                                      Validations.validateUserName2,
                                  onSaved: controller.setUserName,
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                TextFieldWidget(
                                  controller: controller.phoneController!.value,
                                  enabled: true,
                                  focusNode: controller.phoneFN,
                                  hintText: 'Phone',
                                  icon: const Icon(
                                    Icons.phone_android_outlined,
                                    color: Colors.black54,
                                  ),
                                  isText: true,
                                  textInputAction: TextInputAction.next,
                                  nextFocusNode: controller.locationFN,
                                  validateFunction: Validations.validateMobile,
                                  onSaved: controller.setPhone,
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                TextFieldWidget(
                                  controller:
                                      controller.locationController!.value,
                                  enabled: true,
                                  focusNode: controller.locationFN,
                                  hintText: 'location',
                                  icon: const Icon(
                                    Icons.location_pin,
                                    color: Colors.black54,
                                  ),
                                  isText: true,
                                  textInputAction: TextInputAction.next,
                                  nextFocusNode: controller.emailFN,
                                  validateFunction:
                                      Validations.validateCountryName,
                                  onSaved: controller.setLocation,
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                TextFieldWidget(
                                  controller: controller.emailController.value,
                                  enabled: true,
                                  focusNode: controller.emailFN,
                                  hintText: 'Email',
                                  icon: const Icon(
                                    Icons.email_outlined,
                                    color: Colors.black54,
                                  ),
                                  textInputAction: TextInputAction.next,
                                  isEmail: true,
                                  isText: false,
                                  nextFocusNode: controller.passFN,
                                  validateFunction: Validations.validateEmail,
                                  onSaved: controller.setEmail,
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                TextFieldWidget(
                                  controller: controller.passController!.value,
                                  enabled: true,
                                  focusNode: controller.passFN,
                                  hintText: 'Password',
                                  icon: const Icon(
                                    Icons.password,
                                    color: Colors.black54,
                                  ),
                                  isEmail: false,
                                  isPassword: true,
                                  textInputAction: TextInputAction.next,
                                  nextFocusNode: controller.repassFN,
                                  validateFunction:
                                      Validations.validatePassword,
                                  onSaved: controller.setPass,
                                  // submitAction: ,
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                TextFieldWidget(
                                  controller:
                                      controller.repassController!.value,
                                  enabled: true,
                                  focusNode: controller.repassFN,
                                  hintText: 'renter Password',
                                  icon: const Icon(
                                    Icons.password_outlined,
                                    color: Colors.black54,
                                  ),
                                  // is: false,
                                  textInputAction: TextInputAction.done,
                                  isPassword: true,
                                  validateFunction: controller.validteRePass,
                                  onSaved: controller.setRePass,
                                  submitAction: controller.signup,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 50, vertical: 4),
                                  child: AnimeatedButtons(
                                      text: 'Signup',
                                      ontap: () {
                                        FocusScope.of(context).unfocus();
                                        controller.signup();
                                      }),
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                              ],
                            ),
                          );
                        }),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
    BuildContext context,
    Widget child,
    AxisDirection axisDirection,
  ) {
    return child;
  }
}
