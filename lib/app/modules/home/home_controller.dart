import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController
    with GetSingleTickerProviderStateMixin {
  Rx<TabController>? tabController;
  TextEditingController searchController = TextEditingController();
  @override
  void onInit() {
    tabController = TabController(length: 3, vsync: this).obs;
    super.onInit();
  }
}
