import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/modules/create_post/post_model.dart';
import 'package:animal_store/app/modules/home/home_controller.dart';
import 'package:animal_store/app/services/firestore.dart';
import 'package:animal_store/app/widget.dart/pet_widget.dart';
import 'package:animal_store/app/widget.dart/text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:paginate_firestore/paginate_firestore.dart';

class HomeView extends StatefulWidget {
  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final controller = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
          body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              height: Get.height / 18,
              width: Get.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: const [
                    Icon(
                      Icons.location_on,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Syria',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const TextFieldWidget(
              hintText: 'Search pest ..',
              raduis: 10,
              icon: Icon(
                Icons.search,
                color: mainColor,
              ),
            ),
            SizedBox(
              height: Get.height / 20,
              width: Get.width,
              child: TabBar(
                controller: controller.tabController!.value,
                unselectedLabelColor: Colors.white,
                indicatorWeight: 2,
                indicatorColor: mainColor,
                tabs: const <Widget>[
                  Tab(
                    child: Text(
                      'Birds',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Cat',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Dog',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: controller.tabController!.value,
                children: [
                  PaginateFirestore(
                    onEmpty: const Center(child: Text('No bird found')),
                    itemBuilderType: PaginateBuilderType.listView,
                    itemBuilder: (context, documentSnapshots, index) {
                      final data = documentSnapshots[index].data()
                          as Map<String, dynamic>;

                      return PetsWidget(postModel: PostModel.fromJson(data));
                    },
                    query: postsRef
                        .where('category', isEqualTo: 'bird')
                        .orderBy('creatAt', descending: true),
                    itemsPerPage: 10,
                    isLive: true,
                  ),
                  PaginateFirestore(
                    onEmpty: const Center(child: Text('No cat found')),
                    itemBuilderType: PaginateBuilderType.listView,
                    itemBuilder: (context, documentSnapshots, index) {
                      final data = documentSnapshots[index].data()
                          as Map<String, dynamic>;
                      return PetsWidget(postModel: PostModel.fromJson(data));
                    },
                    query: postsRef
                        .where('category', isEqualTo: 'cat')
                        .orderBy('creatAt'),
                    itemsPerPage: 10,
                    isLive: true,
                  ),
                  PaginateFirestore(
                    onEmpty: const Center(child: Text('No dog found')),
                    itemBuilderType: PaginateBuilderType.listView,
                    itemBuilder: (context, documentSnapshots, index) {
                      final data = documentSnapshots[index].data()
                          as Map<String, dynamic>;
                      return PetsWidget(postModel: PostModel.fromJson(data));
                    },
                    query: postsRef
                        .where('category', isEqualTo: 'dog')
                        .orderBy('creatAt'),
                    itemsPerPage: 10,
                    isLive: true,
                  )
                ],
              ),
            ),
          ],
        ),
      ));
    });
  }
}
