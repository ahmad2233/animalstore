import 'package:animal_store/app/services/firestore.dart';
import 'package:get/get.dart';

class MypetsController extends GetxController {
  deletePost(String docID) {
    postsRef.doc(docID).delete();
  }
}
