import 'package:get/get.dart';

import './mypets_controller.dart';

class MypetsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MypetsController>(
      () => MypetsController(),
    );
  }
}
