import 'package:animal_store/app/services/firebase_auth.dart';
import 'package:animal_store/app/widget.dart/my_pets_widget.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:paginate_firestore/paginate_firestore.dart';

import '../../services/firestore.dart';
import '../create_post/post_model.dart';
import './mypets_controller.dart';

class MypetsView extends GetView<MypetsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   // title: const Text(
      //   //   'PET SHELTER',
      //   //   style: TextStyle(color: mainColor),
      //   // ),
      //   leading: IconButton(
      //       onPressed: () {
      //         Get.back();
      //       },
      //       icon: const Icon(
      //         Icons.arrow_back_outlined,
      //         color: mainColor,
      //       )),
      //   backgroundColor: Colors.white,
      //   elevation: 0,
      // ),
      body: SafeArea(
        child: SizedBox.expand(
          child: PaginateFirestore(
            onEmpty: const Center(child: Text('Dont have any pet for sale')),
            itemBuilderType: PaginateBuilderType.listView,
            itemBuilder: (context, documentSnapshots, index) {
              final data =
                  documentSnapshots[index].data() as Map<String, dynamic>;

              return MypetsWidget(
                postModel: PostModel.fromJson(data),
                id: documentSnapshots[index].id,
              );
            },
            query: postsRef
                .where('owner', isEqualTo: firebaseAuth.currentUser!.uid)
                .orderBy('creatAt'),
            itemsPerPage: 10,
            isLive: true,
          ),
        ),
      ),
    );
  }
}
