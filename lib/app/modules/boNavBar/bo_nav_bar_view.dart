import 'package:animal_store/app/routes/app_pages.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';

import '../../data/const.dart';
import '../../widget.dart/fab_container.dart';
import './bo_nav_bar_controller.dart';

class BoNavBarView extends GetView<BoNavBarController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        floatingActionButtonLocation: controller.fapLocation.value,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            controller.fapLocation.value ==
                FloatingActionButtonLocation.endFloat;
            Get.toNamed(Routes.CHOISE);
          },
          backgroundColor: Colors.white,
          child: SvgPicture.asset(
            'assets/svg/cat1.svg',
            color: mainColor,
          ),
        ),
        body: SafeArea(
          child: PageTransitionSwitcher(
            transitionBuilder: (
              Widget child,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
            ) {
              return FadeThroughTransition(
                animation: animation,
                secondaryAnimation: secondaryAnimation,
                child: child,
              );
            },
            child: controller.pages[controller.index.value]['page'],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(width: 5),
              for (Map item in controller.pages)
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: IconButton(
                      icon: Icon(
                        item['icon'],
                        color: item['index'] != controller.index.value
                            ? Colors.grey
                            : mainColor,
                        size: 25.0,
                      ),
                      onPressed: () =>
                          controller.navigationTapped(item['index']),
                    ),
                  ),
                ),
              const SizedBox(width: 5),
            ],
          ),
        ),
      );
    });
  }

  buildFab() {
    return const SizedBox(
      height: 45.0,
      width: 45.0,
      // ignore: missing_required_param
      child: FabContainer(
        icon: Icons.add,
        mini: true,
      ),
    );
  }
}
