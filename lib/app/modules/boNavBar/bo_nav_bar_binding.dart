import 'package:get/get.dart';

import './bo_nav_bar_controller.dart';

class BoNavBarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BoNavBarController>(
      () => BoNavBarController(),
    );
  }
}
