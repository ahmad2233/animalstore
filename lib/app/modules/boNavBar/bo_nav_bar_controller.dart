import 'package:animal_store/app/modules/home/home_view.dart';
import 'package:animal_store/app/modules/mypets/mypets_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../account/account_view.dart';
import '../chat/chat_view.dart';

class BoNavBarController extends GetxController {
  RxInt index = 0.obs;
  Rx<FloatingActionButtonLocation> fapLocation =
      FloatingActionButtonLocation.centerDocked.obs;
  List pages = [
    {
      'title': 'Home',
      'icon': Icons.home,
      'page': HomeView(),
      'index': 0,
    },
    {
      'title': 'Search',
      'icon': Icons.catching_pokemon,
      'page': MypetsView(),
      'index': 1,
    },

    {
      'title': 'Messages',
      'icon': Icons.message_outlined,
      'page': ChatView(),
      'index': 2,
    },

    //   {
    //   'title': 'Notification',
    //   'icon': CupertinoIcons.bell_solid,
    //   'page': Activities(),
    //   'index': 3,
    // },
    {
      'title': 'Profile',
      'icon': Icons.person,
      'page': AccountView(),
      // 'icon': Icons.person,
      // 'page': Profile(profileId: firebaseAuth.currentUser.uid),
      'index': 3,
    },
  ];

  void navigationTapped(int val) {
    index.value = val;
  }
}
