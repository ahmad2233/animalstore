import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../../data/const.dart';
import '../../services/firestore.dart';
import '../../tools/message_type.dart';
import '../../tools/snackbar.dart';
import '../chat/chat_model.dart';

class ConversationController extends GetxController with GetSingleTickerProviderStateMixin {
  String? userId;
  String? chatId;
  String? docChatId;
  FocusNode focusNode = FocusNode();
  Rx<ScrollController> scrollController = ScrollController().obs;
  TextEditingController messageController = TextEditingController();
  RxBool isFirst = false.obs;
  bool isBlocked = false;
  String? newChatId;
  FirebaseAuth auth = FirebaseAuth.instance;
  User? user;
  final picker = ImagePicker();
    bool uploadingImage = false;
  File? image;
  @override
  void onInit() {
    user = auth.currentUser;
    userId= Get.arguments[0];
    docChatId= Get.arguments[1];
    chatId= Get.arguments[2];
    scrollController.value.addListener(() {
      focusNode.unfocus();
    });
    if (chatId == '1') {
      isFirst.value = true;
      print(isFirst.value );
    }

    messageController.addListener(() {
      if (focusNode.hasFocus && messageController.text.isNotEmpty) {
        // setTyping(true);
      } else if (!focusNode.hasFocus ||
          (focusNode.hasFocus && messageController.text.isEmpty)) {
        // setTyping(false);
      }
    });
    super.onInit();
  }

  Stream<DocumentSnapshot<Object?>> userDetalisStream(){
  return  usersRef.doc(userId).snapshots();
  }


  showPhotoOptions() {
    Get.bottomSheet(Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            child: ListTile(
              
              title: const Text("Camera"),
              onTap: () {
                Get.back();
                sendMessageOpreatin(imageType: 0, isImage: true);
              },
            
            ),
          ),
          Flexible(
            child: ListTile(
              title: const Text("Gallery"),
              onTap: () {
               Get.back();
                sendMessageOpreatin(imageType: 1, isImage: true);
              },
            ),
          ),
        ],
      ),
    ));
  }

  sendMessageOpreatin({bool isImage = false, int? imageType}) async {
    scrollController.value.animateTo(0.0,
                                  duration: const Duration(milliseconds: 300),
                                  curve: Curves.easeOut);
    String msg;
    if (isImage) {
      msg = await pickImage(
        source: imageType,
 
        chatID: docChatId,
      );
    } else {
      msg = messageController.text.trim();
      messageController.clear();
    }

    Message message = Message(
        content: msg,
        senderUid: user?.uid,
        reciverUid: userId,
        type: isImage ? MessageType.IMAGE : MessageType.TEXT,
        time: Timestamp.now(),
        isread: false);

    if (msg.isNotEmpty) {
      if (isFirst.value ) {
        String id = await sendFirstMessage(message, userId);
          newChatId = id;
          docChatId = id;
        isFirst.value  = false;
      
      } else {
        print(newChatId);
        if (newChatId == null) {
          sendMessage(
            message,
            docChatId!,
          );
        } else {
          sendMessage(
            message,
            newChatId!,
          );
        }
      }
    }
  }

  sendMessage(Message message, String chatId) async {
    print(message);
    await chatRef.doc(chatId).collection("messages").add(message.toJson());
    await chatRef.doc(chatId).update({"lastTextTime": Timestamp.now()});
  }

  Query messageListStream(String documentId) {
    return chatRef
        .doc(documentId)
        .collection('messages')
        .orderBy('time', descending: true);
  }

  setUserRead() async {
    if(docChatId!.isNotEmpty){
      print(docChatId);
    print(userId);
    WriteBatch batch = FirebaseFirestore.instance.batch();
    await chatRef
        .doc(docChatId)
        .collection('messages')
       .where('senderUid', isEqualTo: userId)
        .where('isread', isEqualTo: false)
        .get()
        .then((response) => {
          inspect(response.docs),
              if (response.docs.isNotEmpty)
                {
                  response.docs.forEach((doc) => {
                        batch.update(doc.reference, {'isread': true})
                      }),
                  batch.commit().then((value) => {print('update')}).catchError(
                      (error) => {print("Failed to update user: $error")}),
                }
            });
    }
    
  }

  Future<String> sendFirstMessage(Message? message, String? recipient) async {
    DocumentReference ref = await chatRef.add({
      'lastTextTime': Timestamp.now(),
      'users': 
 [       recipient,
         user!.uid,]
      ,
      'st': user!.uid,
      're': recipient,
    });
    await sendMessage(message!, ref.id);
    return ref.id;
  }

  pickImage({int? source,String? chatID,BuildContext? context}) async {
    XFile? pickedFile = source == 0
        ? await picker.pickImage(
            imageQuality: 50,
            source: ImageSource.camera,
          )
        : await picker.pickImage(
            imageQuality: 50,
            source: ImageSource.gallery,
          );

    if (pickedFile != null) {
      File? croppedFile = await ImageCropper().cropImage(
          sourcePath: pickedFile.path,
          aspectRatioPresets:
               [
                  CropAspectRatioPreset.square,
                ]
             compressQuality: 50,
          androidUiSettings: const AndroidUiSettings(
            statusBarColor: mainColor,
            cropFrameColor: mainColor,
            activeControlsWidgetColor: mainColor,
            toolbarTitle: 'Edit Image',
            toolbarColor: Colors.white,
            toolbarWidgetColor: mainColor,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false,
          ),
          iosUiSettings: const IOSUiSettings(
            minimumAspectRatio: 1.0,
          ),
        );

      final bytesImage = croppedFile?.readAsBytesSync().lengthInBytes;
      final kbImage = bytesImage !/ 1024;
      final mbImage = kbImage / 1024;
      if (mbImage > 1) {
        showInSnackBar('',"Image too big",false);
      } else {
        if (croppedFile != null) {
          uploadingImage = true;
          image = croppedFile;

          showInSnackBar('',"Uploading image...", false);
          String imageUrl = await uploadImage(croppedFile, chatId!);
          return imageUrl;
        }
      }
    }

  }
    Future<String> uploadImage(File image, String chatId) async {
    Reference storageReference =
        storage.ref().child("chats").child(chatId);
    UploadTask uploadTask = storageReference.putFile(image);
    await uploadTask.whenComplete(() => null);
    String imageUrl = await storageReference.getDownloadURL();
    return imageUrl;
  }


}
