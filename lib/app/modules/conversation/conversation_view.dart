import 'package:animal_store/app/modules/login/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:paginate_firestore/paginate_firestore.dart';

import '../../data/const.dart';
import '../../services/firestore.dart';
import '../../widget.dart/chat_bubble.dart';
import '../chat/chat_model.dart';
import './conversation_controller.dart';
import 'package:timeago/timeago.dart' as timeago;

class ConversationView extends GetView<ConversationController> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await controller.setUserRead();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: () async {
              await controller.setUserRead();
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.keyboard_backspace,
            ),
          ),
          backgroundColor: mainColor,
          elevation: 0.0,
          titleSpacing: 0,
          title: buildUserName(controller.userId!),
        ),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Obx(() {
            print('rrrrebuild');
            return Column(
              children: [
                Expanded(
                    child: controller.isFirst.value != true
                        ? PaginateFirestore(
                            reverse: true,
                            isLive: true,
                            scrollController: controller.scrollController.value,
                            initialLoader: const Center(
                              child: CircularProgressIndicator(
                                color: Colors.transparent,
                              ),
                            ),
                            bottomLoader: const Center(
                              child: CircularProgressIndicator(
                                color: Colors.transparent,
                              ),
                            ),

                            physics: const BouncingScrollPhysics(),
                            query: controller.newChatId == null
                                ? controller
                                    .messageListStream(controller.docChatId!)
                                : controller
                                    .messageListStream(controller.newChatId!),
                            itemsPerPage: 50,
                            shrinkWrap: true,
                            onEmpty: const Center(
                              child: Text('Start Chat'),
                            ),
                            onLoaded: (page) {},
                            // emptyDisplay: Offstage(),

                            itemBuilderType: PaginateBuilderType.listView,
                            itemBuilder: (context, snap, index) {
                              // viewModel.setReadCount(
                              //     widget.docChatId, user, snap.length);

                              Message message = Message.fromJson(
                                  snap[index].data() as Map<String, dynamic>);
                              return ChatBubble(
                                  message: '${message.content}',
                                  time: message.time,
                                  isMe:
                                      message.senderUid == controller.user?.uid,
                                  type: message.type);
                            },
                          )
                        : const Center(child: Text('Start Chat'))
                    // StreamBuilder<QuerySnapshot>(
                    //     stream: chatRef
                    //         .doc()
                    //         .collection('messages')
                    //         // .orderBy(
                    //         //   'time',
                    //         // )
                    //         .snapshots(),
                    //     builder: (context, snapshot) {
                    //       if (snapshot.hasData) {
                    //         List messages = snapshot.data!.docs;
                    //         return ListView.builder(
                    //           controller: controller.scrollController,
                    //           padding: const EdgeInsets.symmetric(
                    //               horizontal: 10.0),
                    //           itemCount: messages.length,
                    //           reverse: true,
                    //           itemBuilder: (BuildContext context, int index) {
                    //             Message message = Message.fromJson(
                    //                 messages.reversed.toList()[index].data());
                    //             return ChatBubble(
                    //                 message: '${message.content}',
                    //                 time: message.time,
                    //                 isMe: message.senderUid ==
                    //                     controller.user?.uid,
                    //                 type: message.type);
                    //           },
                    //         );
                    //       } else if (snapshot.hasError) {
                    //         return Container();
                    //       } else {
                    //         return Center(child: circularProgress(context));
                    //       }
                    //     },
                    //   ),
                    ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: BottomAppBar(
                    elevation: 10.0,
                    child: Container(
                      constraints: const BoxConstraints(maxHeight: 100.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: const Icon(
                              CupertinoIcons.photo_on_rectangle,
                              color: mainColor,
                            ),
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                              controller.showPhotoOptions();
                            },
                          ),
                          Flexible(
                            child: TextField(
                              controller: controller.messageController,
                              // focusNode: controller.focusNode,
                              style: const TextStyle(
                                  fontSize: 15.0, color: mainColor),
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.all(10.0),
                                enabledBorder: InputBorder.none,
                                border: InputBorder.none,
                                hintText: "Type your message",
                                hintStyle: TextStyle(color: mainColor),
                              ),
                              maxLines: null,
                            ),
                          ),
                          IconButton(
                            icon: const Icon(
                              Icons.send,
                              color: mainColor,
                            ),
                            onPressed: () {
                              if (controller
                                  .messageController.text.isNotEmpty) {
                                controller.sendMessageOpreatin();
                              }
                              // controller.scrollController.animateTo(0.0,
                              //     duration: const Duration(milliseconds: 300),
                              //     curve: Curves.easeOut);
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            );
          }),
        ),
      ),
    );
  }
}

buildUserName(String userID) {
  return StreamBuilder<DocumentSnapshot>(
    stream: usersRef.doc(userID).snapshots(),
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        DocumentSnapshot<Object?>? documentSnapshot = snapshot.data;
        UsersModel user = UsersModel.fromJson(
            documentSnapshot!.data() as Map<String, dynamic>);
        return InkWell(
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Hero(
                  tag: user.email!,
                  child: const CircleAvatar(
                    radius: 25.0,
                    // backgroundImage: CachedNetworkImageProvider(
                    //   user.photo!,
                    // ),
                  ),
                ),
              ),
              const SizedBox(width: 10.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '${user.username}',
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),
                    ),
                    const SizedBox(height: 5.0),
                    Text(
                      _buildOnlineText(user, false),
                      style: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 11,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          onTap: () {},
        );
      } else {
        return const Center(child: CircularProgressIndicator());
      }
    },
  );
}

_buildOnlineText(
  UsersModel? user,
  bool typing,
) {
  if (user!.isOnline!) {
    // if (typing) {
    //   return "typing...";
    // } else {
    return "online";
    // }
  } else {
    return 'last seen ${timeago.format(user.lastSeen!.toDate())}';
  }
}
