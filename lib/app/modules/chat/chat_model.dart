import 'package:cloud_firestore/cloud_firestore.dart';
import '../../tools/message_type.dart';

class Message {
  String? content;
  String? senderUid;
  String? reciverUid;
  MessageType? type;
  Timestamp? time;
  bool? isread;

  Message(
      {this.content,
      this.senderUid,
      this.reciverUid,
      this.type,
      this.time,
      this.isread});

  Message.fromJson(Map<String, dynamic> json) {
    content = json['content'];
    senderUid = json['senderUid'];
    reciverUid = json['reciverUid'];
    isread = json['isread'];
    if (json['type'] == 'text') {
      type = MessageType.TEXT;
    } else {
      type = MessageType.IMAGE;
    }
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['content'] = content;
    data['senderUid'] = senderUid;
    data['reciverUid'] = reciverUid;
    data['isread'] = isread;
    if (type == MessageType.TEXT) {
      data['type'] = 'text';
    } else {
      data['type'] = 'image';
    }
    data['time'] = time;
    return data;
  }
}
