import 'dart:developer';

import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/modules/chat/chat_controller.dart';
import 'package:animal_store/app/modules/chat/chat_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../tools/indector.dart';
import '../../widget.dart/chat_item.dart';

class ChatView extends StatefulWidget {
  @override
  State<ChatView> createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {
  final controller = Get.put(ChatController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Chats'),
          centerTitle: true,
          backgroundColor: mainColor,
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: controller.steamChatColloction(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List chatList = snapshot.data!.docs;
              inspect(chatList);
              if (chatList.isNotEmpty) {
                return ListView.separated(
                  itemCount: chatList.length,
                  itemBuilder: (BuildContext context, int index) {
                    DocumentSnapshot chatListSnapshot = chatList[index];

                    return StreamBuilder<QuerySnapshot>(
                      stream: controller.messageListStream(chatListSnapshot.id),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          List messages = snapshot.data!.docs;
                          if (messages.isNotEmpty) {
                            Message message = Message.fromJson(
                              messages.first.data(),
                            );
                            inspect(chatListSnapshot);
                            List users = chatListSnapshot.get('users');
                            users.remove(controller.myUID);
                            String? recipient;
                            recipient = users[0];
                            return ChatItem(
                              userId: recipient,
                              messageCount: messages.length,
                              msg: message.content,
                              time: message.time,
                              chatId: chatListSnapshot.id,
                              type: message.type,
                              currentUserId: controller.myUID,
                            );
                          } else {
                            return const SizedBox();
                          }
                        } else {
                          return const SizedBox();
                        }
                      },
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Align(
                      alignment: Alignment.centerRight,
                      child: SizedBox(
                        height: 0.5,
                        width: MediaQuery.of(context).size.width / 1.3,
                        child: const Divider(),
                      ),
                    );
                  },
                );
              } else {
                return const Center(child: Text('No Chats'));
              }
            } else {
              return Center(child: circularProgress(context));
            }
          },
        ));
  }
}
