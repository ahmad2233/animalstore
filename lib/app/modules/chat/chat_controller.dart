import 'package:animal_store/app/routes/app_pages.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

import '../../services/firebase_auth.dart';
import '../../services/firestore.dart';

class ChatController extends GetxController {
  String? myUID;

  Stream<QuerySnapshot>? userChatsStream;

  Stream<QuerySnapshot<Object?>> steamChatColloction() {
    return chatRef
        .where('users', arrayContains: myUID)
        .orderBy('lastTextTime', descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> messageListStream(String documentId) {
    return chatRef
        .doc(documentId)
        .collection('messages')
        .orderBy('time', descending: true)
        .snapshots();
  }

  startChat(String resciver) async {
    String sender = myUID!;
    print(sender);
    print(resciver);
    QuerySnapshot snapshot = await chatRef
        .where('re', isEqualTo: sender)
        .where('st', isEqualTo: resciver)
        .get();
    if (snapshot.docs.isNotEmpty) {
      Get.toNamed(Routes.CONVERSATION, arguments: [resciver, '', '0']);
    } else {
      QuerySnapshot snapshot = await chatRef
          .where('st', isEqualTo: sender)
          .where('re', isEqualTo: resciver)
          .get();
      if (snapshot.docs.isEmpty) {
        Get.toNamed(Routes.CONVERSATION, arguments: [resciver, '', '1']);
      } else {
        Get.toNamed(Routes.CONVERSATION,
            arguments: [resciver, snapshot.docs[0].id, '0']);
      }
    }
  }

  @override
  void onInit() {
    myUID = firebaseAuth.currentUser?.uid;
    super.onInit();
  }
}
