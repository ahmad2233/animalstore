import 'package:flutter/material.dart';

import 'package:get/get.dart';

import './splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          controller.text.value,
          style: const TextStyle(fontSize: 35),
        ),
      ),
    );
  }
}
