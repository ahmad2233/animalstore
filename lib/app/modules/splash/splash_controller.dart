import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  RxString text = 'PET SHELTER'.obs;
  @override
  void onInit() {
    Timer(const Duration(seconds: 2), () async {
      FirebaseAuth firebaseAuth = FirebaseAuth.instance;
      if (firebaseAuth.currentUser == null) {
        Get.offAllNamed('/login');
      } else {
        Get.offAllNamed('/bonavbar');
      }
    });
    super.onInit();
  }
}
