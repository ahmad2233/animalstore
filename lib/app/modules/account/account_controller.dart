import 'dart:io';

import 'package:animal_store/app/services/firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../../data/const.dart';
import '../../services/firebase_auth.dart';
import '../../tools/snackbar.dart';

class AccountController extends GetxController {
  RxBool loadingImage = false.obs;
  RxBool loadingUploadImage = false.obs;
  RxBool uploadingImage = false.obs;
    File? mediaUrl;
  final picker = ImagePicker();
  String? username;
  String? email;
  String? phone;
  String? location;
  String photoURL = '';
  RxInt radioVal = 2.obs;
  final box = GetStorage();
  User? user;
  @override
  void onInit() async {
    user = firebaseAuth.currentUser;
    username = box.read('username');
    location = box.read('location');
    phone = box.read('phone');
    email = box.read('email');
    radioVal.value = box.read('lang') ?? 1;
    super.onInit();
  }

  setLanVal(int val) {
    box.write('lang', val);
    print(box.read('lang'));
  }

  pickImage(
      {bool camera = false,}) async {
    loadingImage.value = true;
    try {
      XFile? pickedFile = await picker.pickImage(
        
        source: camera ? ImageSource.camera : ImageSource.gallery,
      );
      if (pickedFile != null) {
        File ?croppedFile = await ImageCropper().cropImage(
          sourcePath: pickedFile.path,
          aspectRatioPresets:
               [
                  CropAspectRatioPreset.square,
                ]
               compressQuality: 50,
          androidUiSettings: const AndroidUiSettings(
            statusBarColor: mainColor,
            cropFrameColor: mainColor,
            activeControlsWidgetColor: mainColor,
            toolbarTitle: 'Edit Image',
            toolbarColor: Colors.white,
            toolbarWidgetColor: mainColor,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false,
          ),
          iosUiSettings: const IOSUiSettings(
            minimumAspectRatio: 1.0,
          ),
        );
      if(croppedFile != null){
        mediaUrl = File(croppedFile.path);
      final bytesImage = croppedFile.readAsBytesSync().lengthInBytes;
      final kbImage = bytesImage / 1024;
      final mbImage = kbImage / 1024;
      if (mbImage > 1) {
        showInSnackBar('',"Image too big",false);
      } else { updatePhoto();}
        uploadingImage.value = false;
 
      } else {
        uploadingImage.value = false;
     
      }
      }

    } catch (e) {

   print(e);
    }
    // update();
    //  Get.back();
  // if(mediaUrl != null ){
  //   try{
  //     updatePhoto();
  //   }catch(e){
  //     print(e);
  //     loadingImage.value =false;
  //   }
    
  // }

  }

     Future<String> uploadFile(File file ) async {
    String userId = firebaseAuth.currentUser!.uid;
    Reference storageReference =
        storage.ref().child(userId);
    UploadTask uploadTask = storageReference.putFile(file);
    TaskSnapshot downloadUrl = await uploadTask.whenComplete(() => null);
    String url = await downloadUrl.ref.getDownloadURL();
    return url;
  }

    savePostFireSore(String url) async {
    await usersRef.doc(user!.uid).update({
      'photo':url
    }).catchError((e) => print(e));
  }
    updatePhoto() async{
      loadingUploadImage.value =true;
       String photoURL =await uploadFile(mediaUrl!);
        await savePostFireSore(photoURL);
        loadingUploadImage.value =false;
        }

    currentUserId() {
    return firebaseAuth.currentUser?.uid;
  }

    updateLastTime(){
      usersRef.doc(user!.uid).update({
        'isOnline':false,
        'lastSeen': Timestamp.now(),
      });
    }
}
