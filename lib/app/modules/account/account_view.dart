import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/services/firebase_auth.dart';
import 'package:animal_store/app/tools/indector.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import '../../routes/app_pages.dart';
import '../../services/firestore.dart';
import '../login/user_model.dart';
import './account_controller.dart';

class AccountView extends StatefulWidget {
  @override
  State<AccountView> createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  final controller = Get.put<AccountController>((AccountController()));

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return ModalProgressHUD(
        inAsyncCall: controller.loadingUploadImage.value,
        progressIndicator: circularProgress(context),
        child: Scaffold(
          appBar: AppBar(
            title: const Text(
              'PET SHELTER ',
              style: TextStyle(color: mainColor),
            ),
            // centerTitle: true,
            backgroundColor: Colors.white,
          ),
          body: StreamBuilder<Object>(
              stream: usersRef.doc(controller.user!.uid).snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  print('build account');
                  DocumentSnapshot? documentSnapshot =
                      snapshot.data as DocumentSnapshot;
                  UsersModel user = UsersModel.fromJson(
                      documentSnapshot.data() as Map<String, dynamic>);
                  controller.box.write('username', user.username);
                  controller.box.write('phone', user.phone);
                  controller.box.write('location', user.location);
                  controller.box.write('email', user.email);
                  return Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 10),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Stack(
                                children: [
                                  Container(
                                    height: 150,
                                    width: 150,
                                    decoration: BoxDecoration(
                                      color: Colors.white70,
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                          color: Colors.grey.withOpacity(0.5)),
                                    ),
                                    child: user.photo != ''
                                        ? CachedNetworkImage(
                                            imageUrl: user.photo!,
                                            imageBuilder:
                                                (context, imageProvider) =>
                                                    Container(
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: DecorationImage(
                                                  image: imageProvider,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            fit: BoxFit.fitWidth,
                                            placeholder: (context, url) =>
                                                circularProgress(context),
                                            errorWidget:
                                                (context, url, error) =>
                                                    const Center(
                                              child: Text(
                                                'Unable to load Image',
                                                style:
                                                    TextStyle(fontSize: 10.0),
                                              ),
                                            ),
                                          )
                                        : GestureDetector(
                                            onTap: () {},
                                            child: Icon(
                                              Icons.person_add,
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              size: 35,
                                            ),
                                          ),
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    right: 10,
                                    child: GestureDetector(
                                      onTap: () {
                                        imageDialog();
                                      },
                                      child: const Icon(
                                        Icons.add_circle_outlined,
                                        color: mainColor,
                                        size: 35,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Get.toNamed('/info');
                              },
                              child: Center(
                                child: Container(
                                  width: Get.width * 2 / 3,
                                  height: Get.height / 20,
                                  alignment: Alignment.center,
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 20),
                                  child: const Text(
                                    'Edit Personal Info',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.white70,
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                        color: Colors.grey.withOpacity(0.5)),
                                  ),
                                ),
                              ),
                            ),
                            Obx(() {
                              return ListTile(
                                contentPadding: EdgeInsets.zero,
                                title: const Text("English"),
                                leading: Radio(
                                  value: 1,
                                  groupValue: controller.radioVal.value,
                                  onChanged: (value) {
                                    controller.radioVal.value = value as int;
                                    controller.setLanVal(value);
                                  },
                                  activeColor: Colors.green,
                                ),
                              );
                            }),
                            Obx(() {
                              return ListTile(
                                contentPadding: EdgeInsets.zero,
                                title: const Text("Arabic"),
                                leading: Radio(
                                  value: 2,
                                  groupValue: controller.radioVal.value,
                                  onChanged: (value) {
                                    controller.radioVal.value = value as int;
                                    controller.setLanVal(value);
                                  },
                                  activeColor: Colors.green,
                                ),
                              );
                            }),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              child: Text(
                                'Username : ${user.username}',
                                style: const TextStyle(fontSize: 19),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              child: Text(
                                'Email : ${controller.user?.email}',
                                style: const TextStyle(fontSize: 19),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              child: Text(
                                'Phone : ${user.phone}',
                                style: const TextStyle(fontSize: 19),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              child: Text(
                                'Location : ${user.location}',
                                style: const TextStyle(fontSize: 19),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                              child: GestureDetector(
                                onTap: () async {
                                  await controller.updateLastTime();
                                  await firebaseAuth.signOut();
                                  await controller.box.erase();
                                  Get.offAllNamed(Routes.LOGIN);
                                },
                                child: Center(
                                  child: Container(
                                    height: 80,
                                    width: 80,
                                    decoration: BoxDecoration(
                                        color: const Color.fromARGB(
                                            255, 155, 15, 5),
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                          color: Colors.grey.withOpacity(0.5),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              blurRadius: 4,
                                              spreadRadius: 3)
                                        ]),
                                    child: const Icon(
                                      Icons.power_settings_new_outlined,
                                      color: Colors.white,
                                      size: 35,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const Center(
                              child: Text(
                                'Logout',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 155, 15, 5),
                                    fontSize: 20),
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                } else {
                  return const SizedBox();
                }
              }),
        ),
      );
    });
  }

  imageDialog() {
    Get.bottomSheet(Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 20.0),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(
              'Chose Image',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.camera),
            title: const Text('Camera'),
            onTap: () {
              Get.back();
              controller.pickImage(camera: true);
            },
          ),
          ListTile(
            leading: const Icon(Icons.image),
            title: const Text('Image'),
            onTap: () {
              Get.back();
              controller.pickImage();
            },
          ),
        ],
      ),
    ));
  }
}
