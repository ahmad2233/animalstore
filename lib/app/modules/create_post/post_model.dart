import 'package:cloud_firestore/cloud_firestore.dart';

class PostModel {
  String? name;
  String? type;
  String? category;
  String? age;
  String? length;
  String? wight;
  String? location;
  String? price;
  String? description;
  bool? gendre;
  String? owner;
  String? phone;
  String? photoUrl;
  Timestamp? creatAt;

  PostModel(
      {this.name,
      this.type,
      this.category,
      this.age,
      this.length,
      this.wight,
      this.location,
      this.price,
      this.description,
      this.gendre,
      this.owner,
      this.phone,
      this.photoUrl,
      this.creatAt});

  PostModel.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? '';
    type = json['type'] ?? '';
    category = json['category'] ?? '';
    age = json['age'] ?? '';
    length = json['length'] ?? '';
    wight = json['wight'] ?? '';
    location = json['location'] ?? '';
    price = json['price'] ?? '';
    description = json['description'] ?? '';
    gendre = json['gendre'] ?? '';
    owner = json['owner'] ?? '';
    phone = json['phone'] ?? '';
    photoUrl = json['photoUrl'] ?? '';
    creatAt = json['creatAt'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['type'] = type;
    data['category'] = category;
    data['age'] = age;
    data['length'] = length;
    data['wight'] = wight;
    data['location'] = location;
    data['price'] = price;
    data['description'] = description;
    data['gendre'] = gendre;
    data['owner'] = owner;
    data['phone'] = phone;
    data['photoUrl'] = photoUrl;
    data['creatAt'] = creatAt;

    return data;
  }
}
