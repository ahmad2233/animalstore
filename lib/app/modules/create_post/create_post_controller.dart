import 'dart:io';

import 'package:animal_store/app/services/firestore.dart';
import 'package:animal_store/app/tools/snackbar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../../data/const.dart';
import '../../services/firebase_auth.dart';

class CreatePostController extends GetxController {
  File? mediaUrl;
  final picker = ImagePicker();
  RxBool loadingImage =false.obs;
  RxBool loadingPost =false.obs;
  RxBool isFemale =true.obs;
  RxBool isFree =false.obs;
  Rx<GlobalKey<FormState>> formKey = GlobalKey<FormState>().obs;
  Rx<TextEditingController> cateController = TextEditingController().obs;
  Rx<TextEditingController> nameController = TextEditingController().obs;
  Rx<TextEditingController> typeController = TextEditingController().obs;
  Rx<TextEditingController> ageController = TextEditingController().obs;
  Rx<TextEditingController> wightController = TextEditingController().obs;
  Rx<TextEditingController> lengthController = TextEditingController().obs;
  Rx<TextEditingController> locationController = TextEditingController().obs;
  Rx<TextEditingController> descController = TextEditingController().obs;
  Rx<TextEditingController> priceController = TextEditingController().obs;
  String? myUID;
  String? category;
  FirebaseStorage storage = FirebaseStorage.instance;
  final box = GetStorage();

  String? isEmpty(String? val){
    val?.trim();
    if (val == null || val == '') return 'This field is REQUIRED';
    if (val.length> 30) return 'long text';
    return null;
  }

  String? validatePrice(String? val){
    val?.trim();
    if (val == null || val == '' && !isFree.value) return 'This field is REQUIRED';
    if (val.length> 8) return 'Hight price';
    return null;
  
  }

  pickImage(
      {bool camera = false,}) async {
    loadingImage.value = true;
    try {
      XFile? pickedFile = await picker.pickImage(
        
        source: camera ? ImageSource.camera : ImageSource.gallery,
      );
      if (pickedFile != null) {
        File? croppedFile = await ImageCropper().cropImage(
          sourcePath: pickedFile.path,
          aspectRatioPresets:
               [
                  CropAspectRatioPreset.square,
                ]
           compressQuality: 50, 
          androidUiSettings: const AndroidUiSettings(
            statusBarColor: mainColor,
            cropFrameColor: mainColor,
            activeControlsWidgetColor: mainColor,
            toolbarTitle: 'Edit Image',
            toolbarColor: Colors.white,
            toolbarWidgetColor: mainColor,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false,
          ),
          iosUiSettings: const IOSUiSettings(
            minimumAspectRatio: 1.0,
          ),
        );

        mediaUrl = File(croppedFile!.path);
        loadingImage.value = false;
 
      } else {
        loadingImage.value = false;
     
      }
    } catch (e) {

   print(e);
    }
    update();
     Get.back();
  }

  setCate(String val){
    category = val.toLowerCase();
    print(val);
  }

  imageDialog() {
    Get.bottomSheet(Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 20.0),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(
              'Chose Image',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.camera),
            title: const Text('Camera'),
            onTap: () {
              pickImage(camera: true);
            },
          ),
          ListTile(
            leading: const Icon(Icons.image),
            title: const Text('Image'),
            onTap: () {
              pickImage();
            },
          ),
        ],
      ),
    ));
  
  }
    
    Future<String> uploadFile(File file ) async {
    String userId = firebaseAuth.currentUser!.uid;
    Reference storageReference =
        storage.ref().child('posts').child(userId);
    UploadTask uploadTask = storageReference.putFile(file);
    TaskSnapshot downloadUrl = await uploadTask.whenComplete(() => null);
    String url = await downloadUrl.ref.getDownloadURL();
    return url;
  }

    savePostFireSore(String url) async {
    await postsRef.doc().set({
      'category': category!,
      'name': nameController.value.text,
      'type': typeController.value.text,
      'age': ageController.value.text,
      'length': lengthController.value.text,
      'wight': wightController.value.text,
      'location': locationController.value.text,
      'price': priceController.value.text,
      'adopt': isFree.value,
      'description': descController.value.text,
      'gendre': isFemale.value,
      'owner': myUID,
      'phone': box.read('phone')??'',
      'photoUrl': url,
      'creatAt': Timestamp.now(),
    }).catchError((e) => print(e));
  }
    uploadPost() async{
      loadingPost.value = true;
        if(mediaUrl == null){
        showInSnackBar('Error', 'Please Select image', true);
      }else{ 
        FormState? form = formKey.value.currentState;
    form!.save();
    if (!form.validate()) {
      update();
      showInSnackBar(
          'Error', 'Please fix the errors in red before submitting', true);
    } else {
       String photoURL =await uploadFile(mediaUrl!);
        await savePostFireSore(photoURL);
        Get.back();
        }
       
      }
      loadingPost.value = false;
    }

  @override
  void onInit() {
    myUID =firebaseAuth.currentUser!.uid;
    super.onInit();
  }
}
