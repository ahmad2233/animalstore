import 'package:animal_store/app/data/const.dart';
import 'package:animal_store/app/tools/indector.dart';
import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import './create_post_controller.dart';

class CreatePostView extends GetView<CreatePostController> {
  // final createPostController =
  //     Get.lazyPut<CreatePostController>(() => CreatePostController());
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return ModalProgressHUD(
        inAsyncCall: controller.loadingPost.value,
        progressIndicator: circularProgress(context),
        child: Scaffold(
            floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
            floatingActionButtonLocation:
                FloatingActionButtonLocation.startFloat,
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                await controller.uploadPost();
              },
              backgroundColor: Colors.white,
              child: const Icon(
                Icons.done,
                color: mainColor,
              ),
            ),
            appBar: AppBar(
              elevation: 0,
              centerTitle: true,
              backgroundColor: Colors.transparent,
              leading: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: const Icon(
                  Icons.arrow_back_outlined,
                  color: Colors.grey,
                ),
              ),
            ),
            body: ListView(
              // padding: const EdgeInsets.symmetric(horizontal: 15.0),
              children: [
                GetBuilder<CreatePostController>(
                  builder: (_) {
                    return InkWell(
                      onTap: () => controller.imageDialog(),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.width - 40,
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                        ),
                        child: controller.mediaUrl == null
                            ? const Center(
                                child: Icon(
                                  Icons.add_a_photo_outlined,
                                  size: 66,
                                  color: mainColor,
                                ),
                              )
                            : Image.file(
                                controller.mediaUrl!,
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.width - 30,
                                fit: BoxFit.cover,
                              ),
                      ),
                    );
                  },
                ),
                Form(
                  key: controller.formKey.value,
                  child: Column(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: CustomDropdown(
                            hintText: 'Pets category',
                            items: const [
                              'Bird',
                              'Cat',
                              'Dog',
                            ],
                            hintStyle: const TextStyle(
                                color: Colors.grey, fontSize: 15),

                            fieldSuffixIcon: const Icon(
                              Icons.list,
                              color: Colors.white54,
                            ),
                            borderRadius: BorderRadius.circular(30),
                            onChanged: controller.setCate,
                            // listItemStyle: ,
                            controller: controller.cateController.value,
                            fillColor: Colors.transparent,
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 0),
                            selectedStyle: const TextStyle(fontSize: 13),
                            errorBorderSide:
                                const BorderSide(color: Colors.red),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: controller.nameController.value,
                          decoration: inputDecoration(context, 'Name'),
                          validator: controller.isEmpty,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: controller.typeController.value,
                          decoration: inputDecoration(context, 'Type'),
                        ),
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                controller: controller.wightController.value,
                                decoration: inputDecoration(context, 'Wight'),
                                maxLength: 8,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                controller: controller.lengthController.value,
                                decoration: inputDecoration(context, 'Length'),
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                maxLength: 8,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                controller: controller.ageController.value,
                                decoration: inputDecoration(context, 'Age'),
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                validator: controller.isEmpty,
                                maxLength: 8,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: controller.locationController.value,
                          decoration: inputDecoration(context, 'Location'),
                          maxLength: 30,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            // const Expanded(flex: 1, child: Text('Adabt')),
                            Expanded(
                              flex: 2,
                              child: CheckboxListTile(
                                value: controller.isFree.value,
                                title: const Text('Adabt'),
                                onChanged: (val) {
                                  controller.isFree.value = val!;
                                },
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: TextFormField(
                                controller: controller.priceController.value,
                                decoration: inputDecoration(context, 'Price'),
                                validator: controller.isEmpty,
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                maxLength: 8,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: controller.descController.value,
                          decoration: inputDecoration(context, 'Description'),
                          //  maxLength: 500,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.grey[300]),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: () {
                                    controller.isFemale.value = true;
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: controller.isFemale.value
                                            ? Colors.pink
                                            : Colors.transparent),
                                    child: SizedBox.expand(
                                        child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: const [
                                        Text(
                                          'Female',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        Icon(
                                          Icons.female,
                                        ),
                                      ],
                                    )),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: () {
                                    controller.isFemale.value = false;
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: controller.isFemale.value
                                            ? Colors.transparent
                                            : Colors.blueAccent),
                                    child: SizedBox.expand(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: const [
                                          Text(
                                            'Male',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          Icon(
                                            Icons.male,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 80,
                      )
                    ],
                  ),
                )
              ],
            )),
      );
    });
  }

  inputDecoration(BuildContext context, String? hintText) {
    return InputDecoration(
        fillColor: Colors.transparent,
        filled: true,
        hintText: hintText ?? '',
        hintStyle: TextStyle(
          color: Colors.grey[400],
        ),
        contentPadding: const EdgeInsets.symmetric(horizontal: 20.0),
        border: border(context),
        enabledBorder: border(context),
        focusedBorder: focusBorder(context),
        errorStyle: const TextStyle(height: 0.0, fontSize: 0.0));
  }

  border(BuildContext context) {
    return const OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(30.0),
      ),
      borderSide: BorderSide(
        color: Colors.grey,
        width: 0.0,
      ),
    );
  }

  focusBorder(BuildContext context) {
    return OutlineInputBorder(
      borderRadius: const BorderRadius.all(
        Radius.circular(30.0),
      ),
      borderSide: BorderSide(
        color: Colors.black.withOpacity(0.5),
        width: 1.0,
      ),
    );
  }
}
