import 'package:cloud_firestore/cloud_firestore.dart';

class UsersModel {
  String? username;
  String? location;
  String? phone;
  String? email;
  String? photo;
  bool? isOnline;
  Timestamp? lastSeen;
  bool? typeing;

  UsersModel({
    this.username,
    this.location,
    this.phone,
    this.email,
    this.photo,
    this.isOnline,
    this.lastSeen,
    this.typeing,
  });

  UsersModel.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    location = json['location'];
    phone = json['phone'];
    email = json['email'];
    photo = json['photo'];
    isOnline = json['isOnline'];
    lastSeen = json['lastSeen'];
    typeing = json['typeing'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['username'] = username;
    data['location'] = location;
    data['phone'] = phone;
    data['email'] = email;
    data['photo'] = photo;
    data['isOnline'] = isOnline;
    data['lastSeen'] = lastSeen;
    data['typeing'] = typeing;
    return data;
  }
}
