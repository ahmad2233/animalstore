import 'package:animal_store/app/modules/login/user_model.dart';
import 'package:animal_store/app/services/firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../tools/snackbar.dart';

class LoginController extends GetxController {
  RxString? email = ''.obs;
  RxString? pass = ''.obs;
  Rx<TextEditingController> emailController = TextEditingController().obs;
  Rx<TextEditingController>? passController = TextEditingController().obs;
  FocusNode emailFN = FocusNode();
  FocusNode passFN = FocusNode();
  RxBool isLoading = false.obs;
  RxBool isValid = false.obs;
  Rx<GlobalKey<FormState>> loginFormKey = GlobalKey<FormState>().obs;
  Rx<GlobalKey<ScaffoldState>> loginScafold = GlobalKey<ScaffoldState>().obs;
  FirebaseAuth? auth;
  final box = GetStorage();

  setEmail(val) {
    email!.value = val;
  }

  setPass(val) {
    pass!.value = val;
  }

  login() async {
    isLoading.value = true;
    FormState? form = loginFormKey.value.currentState;
    form!.save();
    if (!form.validate()) {
      showInSnackBar(
          'Error', 'Please fix the errors in red before submitting', true);
    } else {
      try {
        print(email!.value + pass!.value);
        await auth!.signInWithEmailAndPassword(
            email: email!.value, password: pass!.value);
        // if (user.credential != null) {
        await getUserData();

        Get.offAllNamed('/bonavbar');
        // }
      } on FirebaseAuthException catch (e) {
        if (e.code == 'user-not-found') {
          showInSnackBar('Error', 'No user found for that email.', true);
        } else if (e.code == 'wrong-password') {
          showInSnackBar(
              'Error', 'Wrong password provided for that user.', true);
        }
      }
    }
    isLoading.value = false;
  }

  getUserData() async {
    DocumentSnapshot doc = await usersRef.doc(auth!.currentUser!.uid).get();
    Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
    saveLocal(UsersModel.fromJson(data));
  }

  saveLocal(UsersModel usersModel) {
    box.write('username', usersModel.username);
    box.write('phone', usersModel.phone);
    box.write('location', usersModel.location);
    box.write('email', usersModel.email);
  }

  @override
  void onInit() {
    auth = FirebaseAuth.instance;
    super.onInit();
  }
}
