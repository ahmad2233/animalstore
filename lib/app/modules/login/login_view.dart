import 'package:animal_store/app/tools/indector.dart';
import 'package:animal_store/app/widget.dart/animated_button.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

import '../../services/validation.dart';
import '../../widget.dart/text_field.dart';
import './login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return ModalProgressHUD(
        inAsyncCall: controller.isLoading.value,
        progressIndicator: circularProgress(context),
        child: Scaffold(
          backgroundColor: Colors.green,
          key: controller.loginScafold.value,
          // appBar: AppBar(
          //   title: Text('LoginView'),
          //   centerTitle: true,
          // ),
          body: ScrollConfiguration(
            behavior: MyBehavior(),
            child: SingleChildScrollView(
              child: Container(
                height: Get.height,
                width: Get.width,
                decoration: const BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Colors.blueAccent, Colors.white],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight)),
                child: Center(
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 6, vertical: 9),
                    margin:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 9),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)),
                    child: Form(
                      key: controller.loginFormKey.value,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          TextFieldWidget(
                            controller: controller.emailController.value,
                            enabled: true,
                            focusNode: controller.emailFN,
                            hintText: 'Email',
                            icon: const Icon(
                              Icons.email_outlined,
                              color: Colors.black54,
                            ),
                            isEmail: true,
                            nextFocusNode: controller.passFN,
                            validateFunction: Validations.validateEmail,
                            onSaved: controller.setEmail,
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          TextFieldWidget(
                            controller: controller.passController!.value,
                            enabled: true,
                            focusNode: controller.passFN,
                            hintText: 'Password',
                            icon: const Icon(
                              Icons.password_outlined,
                              color: Colors.black54,
                            ),
                            isEmail: false,
                            isPassword: true,
                            nextFocusNode: controller.passFN,
                            validateFunction: Validations.validatePassword,
                            onSaved: controller.setPass,
                            submitAction: controller.login,
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 50, vertical: 4),
                            child: AnimeatedButtons(
                                text: 'Login',
                                ontap: () {
                                  FocusScope.of(context).unfocus();
                                  controller.login();
                                }),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          TextButton(
                              onPressed: () {
                                Get.toNamed('/signup');
                              },
                              child: const Text('Create Account'))
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
    BuildContext context,
    Widget child,
    AxisDirection axisDirection,
  ) {
    return child;
  }
}
