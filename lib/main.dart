import 'package:animal_store/app/services/firebase_config.dart';
import 'package:animal_store/app/services/life_cycle.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app/routes/app_pages.dart';
import 'app/services/user_service.dart';

Future<void> main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();

  await Config.initFirebase();

  runApp(
    GetMaterialApp(
      onInit: () {
        WidgetsBinding.instance?.addObserver(
          LifecycleEventHandler(
            detachedCallBack: () => UserService().setUserStatus(false),
            resumeCallBack: () => UserService().setUserStatus(true),
          ),
        );
      },
      title: "Application",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: GoogleFonts.raleway().fontFamily),
    ),
  );
}
